/*
 * weather_station.c
 *
 *  Created on: Dec 1, 2020
 *  Author: Akshay, Ajmi-- ICFOSS
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"

#include "weather_station.h"
#include "util_console.h"
#include "lora.h"
#include "i2c.h"




///*Anemometer timer*/
//
#define WS_MEASURMENT_TIME   		10000  /* wind speed measurement time 10 sec - defined in ms */
#define WS_CONSTANT_MULTIPLIER   	0.0444  /* radius in kilometer */

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

#define RAINGAUGE_PORT			GPIOB
#define RAINGAUGE_PIN			GPIO_PIN_14
#define RAIN_MEMORY_ADD			0x08080008

#define WINDSPEED_PORT			GPIOA
#define WINDSPEED_PIN			GPIO_PIN_11
#define WIND_SPEED_ENABLE_PORT	GPIOB
#define WIND_SPEED_ENABLE_PIN	GPIO_PIN_6

#define BATT_ENABLE_PORT		GPIOB
#define BATT_ENABLE_PIN			GPIO_PIN_2
#define BATTERY_CHANNEL			ADC_CHANNEL_4

#define WIND_DIR_ENABLE_PORT	GPIOA
#define WIND_DIR_ENABLE_PIN		GPIO_PIN_8
#define WIND_DIR_CHANNEL		ADC_CHANNEL_5

#define PRESSURE_ENABLE_PORT	GPIOB
#define PRESSURE_ENABLE_PIN		GPIO_PIN_13
#define PRESSURE_CHANNEL		ADC_CHANNEL_0


#define TEMPHUM_ENABLE_PORT		GPIOB
#define TEMPHUM_ENABLE_PIN		GPIO_PIN_15
#define TEMPERATURE_CHANNEL		ADC_CHANNEL_2
#define HUMIDITY_CHANNEL		ADC_CHANNEL_3



static void rainGaugeTips();
static uint16_t getAccumulatedRainfall();
static void enable(uint8_t);
static void disable(uint8_t);

static uint16_t readAnalogPressureValue(void);
static uint16_t readWindDirectionValue(void);
static void readTempHumValue(uint16_t *temperature, uint16_t *humidity);
static uint16_t getWindSpeed();
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax);


static void windSpeedRotations(void *context);
static void sensorPowerEnable(void);
static void writeToMemory(uint32_t Address, uint32_t data);
static uint16_t readFromMemory(uint32_t Address);

uint16_t rainGaugetipCount = 0; /*   rainCount increment for every interrupt rain fall */
uint8_t dailyCountFlag = RESET;
uint16_t TotalAccumulatedRainfall=0;

uint16_t windSpeedRotationsCount = 0; /*   Anemometer count */
extern TimerEvent_t InterruptTimer; /*   handler for interrupt timer(wind speed calc)  */
extern LoraFlagStatus AppProcessRequest; /*   LORA send process flag   */



/*  readBatteryLevel   */
/**
 * Created on: Nov 20, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay, Ajmi-- ICFOSS
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
static uint16_t readBatteryLevel(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);
	HAL_Delay(10);
	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(BATTERY_CHANNEL);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * (((R1) + (R2)) / (R2))) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	return batteryLevel;
}

/*  rainGaugeInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PA9 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable() {
	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(RAINGAUGE_PORT, RAINGAUGE_PIN, &initStruct);
	HW_GPIO_SetIrq(RAINGAUGE_PORT, RAINGAUGE_PIN, 0, rainGaugeTips);
}

/*  rainGaugeTips  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief callback function interrupt on PA9 pin
 * @note this pin PA9 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
static void rainGaugeTips() {
	rainGaugetipCount++;

}

/*  getAccumulatedRainfall  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval
 *
 **/
static uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = rainGaugetipCount;
	TotalAccumulatedRainfall += rainfall;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}

// rainfall of 1 day accumulated
uint16_t getTotalRainfall(uint8_t downlinkReceived){
	uint16_t TotalRainfall=0;

	if(downlinkReceived == RESET){

		TotalRainfall = TotalAccumulatedRainfall;

	}
	else{
		//TotalRainfall=0;
		TotalAccumulatedRainfall=0;
		dailyCountFlag = RESET;
	}
	writeToMemory(RAIN_MEMORY_ADD, (uint16_t)TotalRainfall);
	return (uint16_t) TotalRainfall;
}

/*  windSpeedInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PB14 is connected to a anemometer Reed switch
 * @param none
 * @retval none
 *
 **/
void windSpeedInterruptEnable() {
	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;


	HW_GPIO_Init(WINDSPEED_PORT, WINDSPEED_PIN, &initStruct);
	HW_GPIO_SetIrq(WINDSPEED_PORT, WINDSPEED_PIN, 0, windSpeedRotations);

	sensorPowerEnable();// Enabling all sensor power 10 seconds prior to reading.
}

/*  windSpeedInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a timer to read anemometer for user defined interval
 * @note WS_MEASURMENT_TIME is the user defined interval
 * @param none
 * @retval none
 *
 **/
void windSpeedTimerEvent() {

	TimerInit(&InterruptTimer, onWindSpeedTimerEvent);
	TimerSetValue(&InterruptTimer, WS_MEASURMENT_TIME);
}

/*  windSpeedRotations  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief callback function interrupt on PB14 pin
 * @note this pin PB14 is connected to a anemometer Reed sensor
 * @param none
 * @retval none
 *
 **/
static void windSpeedRotations(void *context) {
	windSpeedRotationsCount++;
}

/*  getWindSpeed  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief calculate wind speed with number of rotations captured in timer event
 *
 * @param none
 * @retval Wind speed in kmph * 10
 *
 **/
static uint16_t getWindSpeed() {
	float windSpeedKMH = 0;
	//enable(WINDSPEED_POWER);
//	windSpeedRotationsCount = windSpeedRotationsCount / 2;
	windSpeedKMH = (windSpeedRotationsCount * WS_CONSTANT_MULTIPLIER); /* calibration as per the datasheet of the EMCON sensor*/
	windSpeedRotationsCount = 0;

	disable(WINDSPEED_POWER);
	return (uint16_t) (windSpeedKMH * 100);
}
/*  onWindSpeedTimerEvent  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief disable the interrupt on PB14 after wind speed timer event expires and set the lorawan transmission.
 *
 * @param none
 * @retval none
 *
 **/
void onWindSpeedTimerEvent() {
	/*	disable PB14 interrupt to timer event of WS_MEASURMENT_TIME*/
	HAL_GPIO_DeInit(WINDSPEED_PORT, WINDSPEED_PIN);
	AppProcessRequest = LORA_SET;
}


/*  weatherStationGPIO_Init  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief initialize all the gpio
 * @param none
 * @retval none
 *
 **/
void weatherStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HW_GPIO_Init(WIND_DIR_ENABLE_PORT, WIND_DIR_ENABLE_PIN, &initStruct);
	HW_GPIO_Init(TEMPHUM_ENABLE_PORT, TEMPHUM_ENABLE_PIN, &initStruct);
	HW_GPIO_Init(BATT_ENABLE_PORT, BATT_ENABLE_PIN, &initStruct);
	HW_GPIO_Init(PRESSURE_ENABLE_PORT, PRESSURE_ENABLE_PIN, &initStruct);
}

/*  enable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief manual control of gpio
 * @param the gpio to be enabled
 * @retval none
 *
 **/
static void enable(uint8_t pin) {
	switch (pin) {
	case 1:
		HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
		break;
	case 2:
		HAL_GPIO_WritePin(PRESSURE_ENABLE_PORT, PRESSURE_ENABLE_PIN, GPIO_PIN_SET);
		break;
	case 3:
		HAL_GPIO_WritePin(TEMPHUM_ENABLE_PORT, TEMPHUM_ENABLE_PIN, GPIO_PIN_SET);
		break;
	case 4:
		HAL_GPIO_WritePin(WIND_DIR_ENABLE_PORT, WIND_DIR_ENABLE_PIN, GPIO_PIN_SET);
		break;
	case 5:
		HAL_GPIO_WritePin(WIND_SPEED_ENABLE_PORT, WIND_SPEED_ENABLE_PIN, GPIO_PIN_SET);
		break;
	}
}

/*  disable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief manual control of gpio
 * @param the gpio to be disabled
 * @retval none
 *
 **/
static void disable(uint8_t pin) {
	switch (pin) {
	case 1:
		HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
		break;
	case 2:
		HAL_GPIO_WritePin(PRESSURE_ENABLE_PORT, PRESSURE_ENABLE_PIN, GPIO_PIN_RESET);
		break;
	case 3:
		HAL_GPIO_WritePin(TEMPHUM_ENABLE_PORT, TEMPHUM_ENABLE_PIN, GPIO_PIN_RESET);
		break;
	case 4:
		HAL_GPIO_WritePin(WIND_DIR_ENABLE_PORT, WIND_DIR_ENABLE_PIN, GPIO_PIN_RESET);
		break;
	case 5:
		HAL_GPIO_WritePin(WIND_SPEED_ENABLE_PORT, WIND_SPEED_ENABLE_PIN, GPIO_PIN_RESET);
		break;
	}
}


/*  weatherStationInit */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021
 * Author: Akshay, Ajmi
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void weatherStationInit() {

	// MX_I2C1_Init();
	rainGaugeInterruptEnable();
	windSpeedTimerEvent();
	weatherStationGPIO_Init();
	TotalAccumulatedRainfall = readFromMemory(RAIN_MEMORY_ADD);
}

/*  readWeatherStationParameters*/
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021 by Ajmi
 * Author: Akshay, Ajmi
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void readWeatherStationParameters(awsSensors_t *sensor_data) {
	sensor_data->batteryLevel = readBatteryLevel();
	//enable(WINDSPEED_POWER);
	sensor_data->windSpeed = getWindSpeed();
	sensor_data->rainfall = getAccumulatedRainfall();
	enable(WINDDIR_POWER);
	sensor_data->windDirection = readWindDirectionValue();
	sensor_data->pressure = readAnalogPressureValue();
	enable(TEMPHUM_POWER);
	//HAL_Delay(10000);
	readTempHumValue(&sensor_data->temperature, &sensor_data->humidity);
}

/*  readAnalogPressureValue   */
/**
 * Created on: Jun 16, 2022
 * Last Edited: Jun 16, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  read pressure value connected to analog channel 0
 * @param  none
 * @retval battery voltage level
 *
 **/
static uint16_t readAnalogPressureValue(void) {
	uint16_t analogValue = 0; /*   adc reading for battery is stored in the variable  */
	uint16_t atmPressure = 0;
	uint8_t loop = 0;

	/* enable analog reading */
	enable(PRESSURE_POWER);
	HAL_Delay(10000);
	while(loop<10)
	{
	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(PRESSURE_CHANNEL);

	/*map analog values to pressure values */
	atmPressure += map(analogValue,0,4021,300,1100);
loop+= 1;
HAL_Delay(100);
	}
	atmPressure = atmPressure/10;
	/* disable analog reading */
	disable(PRESSURE_POWER);

	return atmPressure;
}

/*  readTempHumValue   */
/**
 * Created on: Jun 16, 2022
 * Last Edited: Jun 16, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  read pressure value connected to analog channel 0
 * @param  none
 * @retval battery voltage level
 *
 **/

static void readTempHumValue(uint16_t *temperature, uint16_t *humidity) {

	union temphum_t{
		uint16_t temperatureRaw;
		uint16_t humidityRaw;
	}temphum_t;


	/* enable analog reading */
	HAL_Delay(5000);
	temphum_t.temperatureRaw = 0;
	//enable(TEMPHUM_POWER);
	//HAL_Delay(100);
	/* read from the temperature sensor channel */
	temphum_t.temperatureRaw = HW_AdcReadChannel(TEMPERATURE_CHANNEL);
	/*map analog values to pressure values */
	*temperature = map(temphum_t.temperatureRaw,0,3810,0,100);// map temperature values to the pointer variable


	temphum_t.humidityRaw = 0;
	/* read from the humidity sensor analog channel*/
	temphum_t.humidityRaw = HW_AdcReadChannel(HUMIDITY_CHANNEL);

	/*map analog values to humidity values */
	*humidity = map(temphum_t.humidityRaw,0,3810,0,100);// map humidity values to the pointer variable

	/* disable analog reading */
	disable(TEMPHUM_POWER);

	}



/*  readWindDirectionValue   */
/**
 * Created on: Jun 17, 2022
 * Last Edited: Jun 17, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  read pressure value connected to analog channel 0
 * @param  none
 * @retval battery voltage level
 *
 **/
static uint16_t readWindDirectionValue(void) {
	uint16_t analogValue = 0; /*   adc reading for battery is stored in the variable  */
	uint16_t windAngle = 0;

	HAL_Delay(1000);
	/* enable analog reading */
	//enable(WINDDIR_POWER);

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(WIND_DIR_CHANNEL);

	/*map analog values to pressure values */
	windAngle = map(analogValue,0,4096,0,360);

	/* disable analog reading */
	disable(WINDDIR_POWER);


	return windAngle;
}


/*  map   */
/**
 * Created on: Jun 21, 2022
 * Last Edited: Jun 21, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  Re-maps a number from one range to another
 * @param  value to be mapped, actual range min, actual range max, new range min, new range max
 * @retval mapped value
 *
 **/
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax){
	return ((((au32_IN - au32_INmin)*(au32_OUTmax - au32_OUTmin))/(au32_INmax - au32_INmin)) + au32_OUTmin);
}

/*
static int32_t map(int32_t au32_IN, int32_t au32_INmin, int32_t au32_INmax, int32_t au32_OUTmin, int32_t au32_OUTmax){
	return ((((au32_IN - au32_INmin)*(au32_OUTmax - au32_OUTmin))/(au32_INmax - au32_INmin)) + au32_OUTmin);
}*/

/*  sensorPowerEnable   */
/**
 * Created on: Jun 28, 2022
 * Last Edited: Jun 28, 2022
 * Author: Ajmi-- ICFOSS
 *
 * @brief  enabling all sensor power(gpio)
 * @param  none
 * @retval none
 *
 **/
static void sensorPowerEnable(void){

	//enable(BATT_POWER);
	//enable(PRESSURE_POWER);
	//enable(WINDDIR_POWER);
	//enable(TEMPHUM_POWER);
	enable(WINDSPEED_POWER);
}


static void writeToMemory(uint32_t Address, uint32_t data)
{
	/*EEPROM DATA STORING*/

	HAL_FLASHEx_DATAEEPROM_Unlock();
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data);
	HAL_FLASHEx_DATAEEPROM_Lock();

}

static uint16_t readFromMemory(uint32_t Address){
	return *(uint32_t *)Address;
}
